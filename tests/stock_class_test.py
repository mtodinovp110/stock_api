from unittest import mock
from unittest.mock import patch

#from pyrsistent import T

from stock_class import Stock, Basket


def test_if_inputs_match_outputs():
                
    a = Stock()   ## instanciate the stock class

    spawn = Basket()

    answer_list = ('soup', 'bread', 'soup', 'apples')

    #a.setListItems("a",5.23)S

    zfile = "tests/catalogue.json"
    xfile = "tests/discounts.json"
    spawn.importJsonCatalogue(zfile)
    spawn.importDiscountsCatalogue(xfile)

    inputs = iter(["soup", "soup", "--end"])

    m = mock.Mock()
    m.side_effect = ['soup', 'bread', 'soup', 'apples', '--end']
    expected_output_1 = ['soup', 'bread', 'soup', 'apples']  ### without 2 for bread applied 

    with patch('builtins.input', m) as mock_method:
        spawn.scanItem()
    
    if spawn.shopping_list == expected_output_1:
        assert True
    else:
        assert False

    spawn.applyDiscGetTotal()

    # soup - 0.65, 0.8, 0.65. 0.9 total should be 3 (plus apples disc)

    if spawn.zsum == 3:
        assert True
    else:
        print(spawn.zsum)
        assert False
    ## expected output 2 after discount is applied and also bread added 
    expected_output_2 = ['soup', 'bread', 'soup', 'apples', 'bread']

    if spawn.shopping_list == expected_output_2:
        assert True
    else:
        print("failed: ",spawn.shopping_list)
        assert False

def test_testing_stock_class():
    spawn = Stock()
    zfile = "tests/catalogue.json"
    xfile = "tests/discounts.json"
    spawn.importJsonCatalogue(zfile)
    spawn.importDiscountsCatalogue(xfile)

    check_catalogue = {
        "soup": 0.65,
        "bread": 0.80,
        "milk": 1.30,
        "apples": 1.00
    }

    check_discounts = {
        "apples": "10 percent discount",
        "soup": "buy 2 get bread free"
    }

    if check_catalogue == spawn.dict:
        assert True
    else:
        assert False

    if check_discounts == spawn.discDict:
        assert True
    else:
        assert False

    key_to_check = "soup"

    catch_return_state = spawn.checkItemExists(key_to_check)

    if catch_return_state == 0:
        assert True
    else:
        assert False
    
    key_to_check = "bread"

    catch_return_state = spawn.checkItemExists(key_to_check)

    if catch_return_state == 0:
        assert True
    else:
        assert False

    key_to_check = "milk"

    catch_return_state = spawn.checkItemExists(key_to_check)

    if catch_return_state == 0:
        assert True
    else:
        assert False

    key_to_check = "apples"

    catch_return_state = spawn.checkItemExists(key_to_check)

    if catch_return_state == 0:
        assert True
    else:
        assert False
