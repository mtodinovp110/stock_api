import json

## Stock() storage class, spawned through inheritance via Basket Definition.
class Stock:
    ''' A Stock Saving Class Or Stock Loading'''
    def __init__(self):
        self.dict = {}       ## catalogue json array
        self.discDict = {}   ## discount json array
        self.zsum = 0        ## sum of the shopping cart with alterations
## checkDiscount() we use this function to check for discounted items and return 0 
## return 1 or 0 depending on find or no find characteristics
## param the_key this is the key we are using to search for
    def checkDiscount(self,the_key):
        for key, value in self.discDict.items():
            if key == the_key:
                #print("Discount Detected for: ",key, "Discount information: ",value)
                return 0
        else:
            return 1
## checkItemExists() similar as check discount, but we check if item is in stock,
## if not in stock series of events will trigger asking user to re-select
## param the_key key we're looking for in the hash table
## return 0 or 1 depending on whether we find item or not
    def checkItemExists(self,the_key):
        for key, value in self.dict.items():
            if key == the_key:
                print(key," Cost: ", value)   ## print me key, print me value, and return 0 (success)
                return 0
        else:
            #print("Attention Shopper, Item: ", the_key, " Was not found! Please Re-select Item")
            return 1
## importJsonCatalogue() used for importing the catalogue from json file
## return void
## path_to_file param used for file location
    def importJsonCatalogue(self,path_to_file):   ## automatic loading of catalogue
        ptr = open(path_to_file,"r")
        self.dict = json.load(ptr)
        ptr.close()
## importDiscountsCatalogue() imports the catalogues
## return void
## param path_to_file the path of the specified file.
    def importDiscountsCatalogue(self,path_to_file):  ## automatic loading of discounts catalogue
        ptr = open(path_to_file,"r")
        self.discDict = json.load(ptr)
        ptr.close()

## Basket class that inherits from stock
class Basket(Stock):
    ''' A Basket Modelling Sub Hierachy based on the Stock Class (inheritance and polymorphism)'''
    def __init__(self):
        Stock.__init__(self)
        self.shopping_list = []

## applyDiscGetTotal applies discount and gets total
## return void
    def applyDiscGetTotal(self):
        self.zsum = 0
        itemflag = ""
        count = 0
        for item in self.shopping_list:
            error_check = self.checkDiscount(item)
            if error_check == 0 and self.discDict[item] == "buy 2 get bread free":
                itemflag = "bread"
                count = count +1
            if error_check == 0 and self.discDict[item] == "10 percent discount":
                print(self.discDict[item], " on ", item, " now £", (self.dict[item] * 0.9))
                self.zsum = self.zsum + (self.dict[item] * 0.9)
            else:
                self.zsum = self.zsum + self.dict[item]
        if count > 0 and ((count % 2) == 0):
            zrange = int(count / 2)
        for i in range (0, zrange):
            self.shopping_list.append(itemflag)
            print("Attention Shopper, you qualify for Free: ", itemflag, " ", i+1, "times")
            if count == 2:
                count = 0
        round(self.zsum,2) ## rounding to 2 decimal places (for a nicer number no x10^-15 results instead x10^-2)
        print("Your total items with discounts/free items applied: ",self.shopping_list)
        print("Total Plus Discounts applied: £",self.zsum)
        

## scanItem() scans items (by console input)
## return void
    def scanItem(self):
        print("Welcome shopper, please write your items as console inputs, when you are done scanning write --end in console to end input.")
        while True:
            value = input("Enter shopping cart item -> ")
            if value == '--end':
                break
            error_check = self.checkItemExists(value)
            if error_check == 0:
                self.shopping_list.append(value)
            else:
                print("Attention shopper, please select one of the following items that are in stock: ",self.dict)
        print(self.shopping_list)

# Uncomment below to run console commands
# you can review the test unit under test folder to see how functions work
# simply uncomment lines 86 to 94 and run to see how it works
# use visual studio code and press run and debug after you have uncommented the below.
# i have commented everything to aid "test driven development"
# please review the test unit to see functionality and additional test sequences.

#a = Basket()

#zfile = "tests/catalogue.json"
#xfile = "tests/discounts.json"
#a.importJsonCatalogue(zfile)
#a.importDiscountsCatalogue(xfile)

#a.scanItem()
#a.applyDiscGetTotal()
